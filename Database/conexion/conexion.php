<?php 

	include __DIR__ . '/../../Config/vars.php';

	
	class Conexion extends Variables
	{
		
		public function conectarCalendario() {
			$connect = mysqli_connect($this->host,$this->user,$this->pass,$this->dbCalendario)
            or die ("Error". mysqli_error($connect));
            return $connect;
		}
		public function ejectuarCalendario($sql) {
			$connect = $this->conectarCalendario();
			$result = $connect->query($sql);
			return $result;
		}
	}


?>