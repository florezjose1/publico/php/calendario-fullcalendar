
<div class="modal fade" tabindex="-1" id="myModal" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <center><h4 class="modal-title">Registrar Evento</h4></center>
      </div>
      <div class="modal-body">
        <form>
          <p>Descripción</p>
          <textarea class="form-control" rows="3" id="descripcion"></textarea>
          <br><br>
          <p>Fecha</p>
          <input type="text" id="fecha" name="fecha" class="form-control">
          <input type="reset" id="reset_event" style="display: none;">
        </form>
      </div>
      <div class="modal-footer">
        <center>
            <button type="button" class="btn btn-success" onclick="saveEvent();">Guardar</button>
            <button type="button" id="no-save" class="btn btn-danger" data-dismiss="modal">Close</button>
        </center>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" id="modal-update-event" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <center><h4 class="modal-title">Update Event</h4></center>
      </div>
      <div class="modal-body">
         <input type="hidden" id="detail-id">
            <p>Description</p>
            <textarea class="form-control" rows="3" id="detail-description"></textarea>
            <br><br>
            <p>Date</p>
            <input type="text" id="detail-date" name="detail-date" class="form-control">
      </div>
      <div class="modal-footer">
         <center>
            <button type="button" class="btn btn-success" id="update" data-toggle="modal" data-target=".modal-update-event">update</button>
            
            <button type="button" class="btn btn-danger" id="delete" data-toggle="modal" data-target=".ModalBorrar" >Delete</button>
            
            <button type="button" class="btn btn-info" id="cerrar_modal_detail" data-dismiss="modal"  >Cancel</button>
            
         </center>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--   modal Actualizar -->
<div class="modal fade modal-update-event" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style=" top: 0px;margin-right: 15px; ">
   <div class="modal-dialog modal-xs">
      <div class="modal-content" style="padding: 100px 10px 105px 10px; background: #3b3b3b;  ">
         <h2 style="color: #fff;  text-align: center;">¿Éstas seguro de Actualizar el Registro?</h2>
         <br><br><br><br>
         <center>
            <button type="hidden" id="update" name="update" onclick="updateDetailEnvent()" class="btn btn-success btn-md" data-dismiss="modal" > ¡Si!</button>
            <button type="hidden" id="no-update" name="update"  data-dismiss="modal" class="btn btn-danger btn-md" >¡No!</button> 
         </center>       
      </div>
   </div>
</div>





<!--   modal Borrar -->
<div class="modal fade ModalBorrar" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style=" top: 0px;margin-right: 15px; ">
  <div class="modal-dialog modal-xs">
    <div class="modal-content" style="padding: 100px 10px 105px 10px; background: #3b3b3b;  ">
        <h2 style="color: #fff; ">Estas seguro de eliminar el registo !!!</h2>
        <br><br><br><br>
        <center>
            <button type="hidden" id="update" name="update" onclick="deleteEvent()" class="btn btn-success btn-md" data-dismiss="modal" > ¡Si!</button>
            <button type="hidden" id="no-delete" name="delete"  data-dismiss="modal" class="btn btn-danger btn-md" >¡No!</button> 
        </center>       
    </div>
  </div>
</div>