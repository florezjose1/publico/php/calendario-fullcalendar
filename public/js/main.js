function formattedDate(date) {
   var d = new Date(date || Date.now()), // tiempo en milisengundos
      month = '' + (d.getMonth() + 1), // mes
      day = '' + d.getDate(), // dia
      year = d.getFullYear(); // año

   if (month.length < 2) month = 1 + parseInt(month);
   if (day.length < 2) day = 1 + parseInt(day);

   if (day < 10 ){
      di = '0'+day;
   }else{
      di = day;
   }
   return [year, month,di].join('-');
}
function events(){
   var param  ={'Funcion':'eventos'};
   $.ajax({
      data: JSON.stringify(param),
      type: "JSON",
      url: 'ajax.php',
      success: function(data) {
         console.log(data)
         json = JSON.parse(data);
         htm = "";
         //console.log(data)
                     
         var initialLangCode = 'es';
         var f = new Date();
         if (f.getDate() < 10 ){
            di = '0'+f.getDate();
         }else{
            di = f.getDate();
         }

         
         if (f.getMonth()<10) {
            fechahoy  =f.getFullYear()+ "-" + '0' + (parseInt(f.getMonth()) + parseInt(1) )+ "-" + di ;
         }else{
            fechahoy  =f.getFullYear()+ "-" + ( parseInt(f.getMonth()) + parseInt(1) )+ "-" + di ;
         }
                     
         cal = $('#calendar_valida').val();

         $('#calendar'+cal).fullCalendar({
            header: {
               left: 'prev,next today',
               center: 'title',  
               right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: $("#calendar").click(),
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            selectHelper: true,

            select: function(start, end) {
               if(formattedDate(start._d) > fechahoy){
                  console.log("start._d", start._d);
                  //console.log("SE PODRA REGISTRAR")
                  $('#myModal').modal('show')
               }else{
                  //console.log('mal')
                  toastr.warning('No puedes Registar aca !', 'Esta fecha ya paso !' , {timeOut: 5000})
               }

               if (start._i[3] != undefined) {
                  hora = start._i[3]+":"+start._i[4]+":"+start._i[5];
               }else{
                  hora ="23:59:59";
               }
               

               if (f.getMonth()<10) {
                  var datefecha = f.getFullYear()+"-"+'0'+(f.getMonth() + 1 )+"-"+start._i[2];
               }else{
                  var datefecha = f.getFullYear()+"-"+(f.getMonth() + 1 )+"-"+start._i[2];
               }


               $('#fecha').val(datefecha+" "+hora)
               //console.log(formattedDate(start._d + ' !!!!!!!!!!11111'))
                                 
               $('#calendar').fullCalendar('unselect');
            },

            eventClick: function(date, jsEvent, view) {
               var cab = date.title;
               var cod = cab.split( ',' );
               
               detail_event(cod[0]) 
            },
            editable: true,
            lang: initialLangCode,
            eventLimit: true, // allow "more" link when too many events
            events: json
         });

         $(".fc-month-button").on( "click", function() {
            // lo que queramos realizar
            $('#calendar_valida_pos').val('month');
            //console.log('month')
         });
                     
         $(".fc-agendaDay-button").on( "click", function() {
               // lo que queramos realizar
            $('#calendar_valida_pos').val('agendaDay');
            //console.log('agendaDay')
         });

         $(".fc-agendaWeek-button").on( "click", function() {
               // lo que queramos realizar
            $('#calendar_valida_pos').val('agendaWeek');
            //console.log('agendaWeek')
         });

         $(".fc-"+$('#calendar_valida_pos').val()+"-button").click();
      }
   });
}
function detail_event(cod){
   var param ={'Funcion':'eventDetalle', 'cod':cod};
   $.ajax({
      data: JSON.stringify (param),
      type:"JSON",
      url: 'ajax.php',
      success:function(data){
         console.log("data", data);

         $('#modal-update-event').modal('show');
         var json = JSON.parse(data);

         var id = json[0].id;
         var descripcion = json[0].title;
         var date = json[0].start;

         $('#detail-id').val(id)
         $('#detail-description').val(descripcion)
         $('#detail-date').val(date)
      }
   });
}
function saveEvent(){
   var fecha = $('#fecha').val();
   var descripcion = $('#descripcion').val();

   if (fecha.length>0 && descripcion.length>0) {
      var param  ={'Funcion':'crudEvents','fecha':fecha,'descripcion':descripcion, 'variable':'1'};
      $.ajax({
         data: JSON.stringify(param),
         type: "JSON",
         url: 'ajax.php',
         success: function(data) {
            console.log(data)
            if (data>0) {
               $('#no-save').click();
               $('#reset_event').click();
               $('#myModal').modal('hide')

               $('#calendar').html('');
               sum = 1 + parseInt($('#calendar_valida').val());
               $('#calendar_valida').val(sum);
               $('#tip').html($('#tip').html('')+ '<div id="calendar'+sum+'"></div>');
               events();
            }else{
               
            }
         }
      });
   }else{
      
   }
}
function updateDetailEnvent(){
   var id          = $('#detail-id').val();
   var descripcion = $('#detail-description').val();
   var date        = $('#detail-date').val();

   var param  ={'Funcion':'crudEvents','fecha':date,'descripcion':descripcion,'id':id, 'variable':'2'};
   $.ajax({
      data: JSON.stringify(param),
      type: "JSON",
      url: 'ajax.php',
      success: function(data) {
         console.log(data)
         if (data>0) {
            $('#no-update').click();
            $('#cerrar_modal_detail').click();

            $('#calendar0').html('');
            sum = 1 + parseInt($('#calendar_valida').val());
            $('#calendar_valida').val(sum);
            $('#tip').html( '<div id="calendar'+sum+'"></div>');
            events();
         }
      }
   });
}
function deleteEvent(){
   var id = $('#detail-id').val();

   var param  ={'Funcion':'crudEvents','id':id, 'variable':'3'};
   $.ajax({
      data: JSON.stringify(param),
      type: "JSON",
      url: 'ajax.php',
      success: function(data) {
         console.log(data)
         if (data>0) {
            $('#no-delete').click();
            $('#cerrar_modal_detail').click();


            $('#calendar0').html('');
            sum = 1 + parseInt($('#calendar_valida').val());
            $('#calendar_valida').val(sum);
            $('#tip').html( '<div id="calendar'+sum+'"></div>');
            events();
         }
      }
   });
}


 