<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Calendar-cafe</title>
	<link rel="stylesheet" href="public/bootstrap/css/fonts.css">
	<link rel="stylesheet" href="public/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href="public/css/style.css">
	<!--<link rel="stylesheet" href="public/bootstrap/css/compiled.css">-->

	<link rel="stylesheet" href="public/Librarys/Calendar/fullcalendar.css">
	<link rel="stylesheet" href="public/Librarys/Calendar/fullcalendar.print.css" media='print' />


</head>
<body>
	
	<div class="nav">
		<center>
			<h1>Calendario</h1>
		</center>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-xs-12"><hr></div>
			<div class="col-xs-12 col-md-offset-2 col-md-8" id="tip">
	        	<div id="calendar0"></div>
			</div>
		</div>

		<div class="col-md-12 col-xs-12" id="tip">
	    </div>
	    <input type="hidden" value="0" id="calendar_valida">
	    <input type="hidden" value="agendaWeek" id="calendar_valida_pos">
	    <div class="col-md-12">     <br><br><br></div>


		<?php include 'public/inc/modales.php'; ?>
		
		
	</div>


	<script src="public/plugins/jquery/jquery.min.js"></script>
	<script src="public/bootstrap/js/bootstrap.min.js"></script>

	<script src="public/js/main.js"></script>

   <script src='public/Librarys/Calendar/moment.min.js?n=1'></script>
   <script src='public/Librarys/Calendar/fullcalendar.min.js?n=1'></script>
   <script src='public/Librarys/Calendar/lang-all.js?n=1'></script>


	<script type="text/javascript">


      $(document).ready(function() {
	      events();
	   });

   </script>

</body>
</html>